using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour
{
    private int id;
    private int movesAmount;

    public int Id {
        get => id;
        private set => id = value;
    } 
    
    public int MovesAmount {
        get => movesAmount;
        private set => movesAmount = value;
    }

    public void Init(int id)
    {
        this.id = id;

        // Reset progress
        movesAmount = 0;
    }

    public void OnPlayerMoved()
    {
        movesAmount++;
    }
}
