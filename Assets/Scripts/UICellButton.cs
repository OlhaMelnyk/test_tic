using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICellButton : MonoBehaviour
{
    [SerializeField] Button button;

    [SerializeField] Image buttonImage;
    
    public int Index { get; private set; }

    private bool isAlredySelected = false;

    public void Init(int index, Transform parent)
    {
        Index = index;

        transform.SetParent(parent);

        // Reset visual
        ReinitCell();
    }

    public void ReinitCell()
    {
        isAlredySelected = false;

        SetSprite(null);
        SetImageColor(Color.white);

        button.interactable = true;
    }

    public void SetSprite(Sprite sprite)
    {
        buttonImage.sprite = sprite;
    }

    public void SetImageColor(Color color)
    {
        buttonImage.color = color;
    }

    public void OnUserSelectedSell()
    {
        isAlredySelected = true;

        button.interactable = false;
    }

    public void ActionButton()
    {
        if (isAlredySelected || !GameController.IsGameActive) return;

        GameController.OnUICellButtonPressed(Index,delegate { 
        
             // Update visual
            SetSprite(UIGamePage.GetCurrentPlayerSprite());
            SetImageColor(UIGamePage.DefaultCellColor);

            OnUserSelectedSell();
        });

    }
}
