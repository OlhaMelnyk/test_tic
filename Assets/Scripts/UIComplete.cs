using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIComplete : MonoBehaviour
{
    private static UIComplete instance;

    [SerializeField] Canvas canvas;
    [SerializeField] Text panelText;

    private static Canvas Canvas => instance.canvas;
    private static Text PanelText => instance.panelText;

    private void Awake()
    {
        instance = this;

        Hide();
    }

    public static void Show(string gameReportMessage)
    {
        Canvas.enabled = true;

        PanelText.text = gameReportMessage;
    }

    public static void Hide()
    {
        Canvas.enabled = false;
    }

    public void ContinueButton()
    {
        GameController.OnContinueButtonPressed();
    }
}
