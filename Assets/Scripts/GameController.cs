using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private static readonly string FIRST_PLAYER = "FIRST PLAYER";
    private static readonly string SECOND_PLAYER = "SECOND PLAYER";

    private static readonly int FRS_PLAYER_ID = 0;
    private static readonly int SEC_PLAYER_ID = 1;

    public static readonly int CELLS_AMOUNT = 9;

    private static GameController instance;

    private static PlayerBehaviour player1;
    private static PlayerBehaviour player2;

    public static List<int> cellsIdList = new List<int>();
    public static int[][] cellsIndexes = { new int[3] { 0, 1, 2}, new int[3] { 3, 4, 5}, new int[3] { 6, 7, 8}, new int[3] { 0, 3, 6}, new int[3] { 1, 4, 7}, new int[3] { 2, 5, 8}, new int[3] { 0, 4, 8}, new int[3] { 2, 4, 6} };
    public static bool IsGameActive { get; private set; }
     
    private static int currentPlayerId;
    private static int busyCellsAmount = 0;


    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        LoadGame();

        UIGamePage.Show();
    }

    public static void LoadGame()
    {
        // 
        IsGameActive = true;

        // 
        player1 = new PlayerBehaviour();
        player2 = new PlayerBehaviour();

        // Init UI
        UIGamePage.InitUICellButtons();
        UIGamePage.UpdatePlayerText(FIRST_PLAYER);

        busyCellsAmount = 0;

        currentPlayerId = FRS_PLAYER_ID;

        // Init players
        player1.Init(FRS_PLAYER_ID);
        player2.Init(SEC_PLAYER_ID);

        // Init id list
        cellsIdList = new List<int>(CELLS_AMOUNT);
       
        for (int i = 0; i < CELLS_AMOUNT; i++)
        {
            cellsIdList.Add(-1);
        }
        
    }

    public static void OnUICellButtonPressed(int index, System.Action onSuccessMove = null)
    {
        // Init cell by id
        if (cellsIdList[index] == -1) // This means cell not inited yet
        {
            cellsIdList[index] = currentPlayerId;
            onSuccessMove?.Invoke();
        }
        else return;   

        Debug.Log($"index = {index}");
        Debug.Log($"currentPlayerId = {currentPlayerId}");

        // Increment moves amount
        if (player1.Id == currentPlayerId) 
        {
            player1.OnPlayerMoved();
        }
        else        
            player2.OnPlayerMoved();
        
        // Need to check draw
        busyCellsAmount++;


        // Check win or draw
        for (int i = 0; i < cellsIndexes.Length; i++)
        {           
            if(IsFinishReached(cellsIndexes[i]))
            {
                break;                
            }
        }

        // Update UI
        if (currentPlayerId.Equals(FRS_PLAYER_ID))
        {
            currentPlayerId = SEC_PLAYER_ID;

            UIGamePage.UpdatePlayerText(SECOND_PLAYER);
        }
        else
        {          
            currentPlayerId = FRS_PLAYER_ID;
            
            UIGamePage.UpdatePlayerText(FIRST_PLAYER);
        }

    }

    public static bool IsFirstPlayerMoved() // This means first player already moved and need to put cross
    {
        return currentPlayerId.Equals(FRS_PLAYER_ID);
    }

    public static bool IsFinishReached(int[] indexes)
    {
        if (indexes.Length < 3)
        {
            Debug.LogWarning("You should set minimum 3 indexes");
            return false;
        }       

        if (cellsIdList[indexes[0]] == FRS_PLAYER_ID && cellsIdList[indexes[1]] == FRS_PLAYER_ID && cellsIdList[indexes[2]] == FRS_PLAYER_ID)
        {
            if (player1.MovesAmount >= 3)
            {
                PlayerWin(indexes);
                return true;
            }
            return false;
        }
        else if (cellsIdList[indexes[0]] == SEC_PLAYER_ID && cellsIdList[indexes[1]] == SEC_PLAYER_ID && cellsIdList[indexes[2]] == SEC_PLAYER_ID)
        {
            if (player2.MovesAmount >= 3)
            {
                PlayerWin(indexes);
                return true;
            }
            return false;
        }
        else if (busyCellsAmount >= CELLS_AMOUNT)
        {
            OnDraw();
            return true;
        }
        else return false;
    }

    private static void PlayerWin(int[] indexes)
    {
        if (currentPlayerId.Equals(FRS_PLAYER_ID))
        {
            Debug.Log("First player win");

            UIComplete.Show("First player win");
        }
        else
        {
            Debug.Log("Second player win");

            UIComplete.Show("Second player win");
        }

        IsGameActive = false;

        UIGamePage.SelectedWinCells(indexes);
    }

    private static void OnDraw()
    {
        IsGameActive = false;

        UIComplete.Show("Draw");
    }

    public static void OnContinueButtonPressed()
    {
        UIComplete.Hide();

        LoadGame();
    }
}
