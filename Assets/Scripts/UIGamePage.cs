using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGamePage : MonoBehaviour
{ 
    private static UIGamePage instance;


    [Header("Settings")]
    [SerializeField] Color defaultCellColor;
    [SerializeField] Color winCellColor;
    [SerializeField] Sprite crossSprite;
    [SerializeField] Sprite circleSprite;

    [Header(" ")]
    [SerializeField] Canvas canvas;

    [Header("Prefab")]
    [SerializeField] GameObject uiButton;
    [Header(" ")]
    [SerializeField] Transform gridParent;

    [Header(" ")]
    [SerializeField] Text playerText;

    private static Sprite CrossSprite => instance.crossSprite;
    private static Sprite CircleSprite => instance.circleSprite;
    public static Color DefaultCellColor => instance.defaultCellColor;
    private static Color WinCellColor => instance.winCellColor;
    private static Canvas Canvas => instance.canvas;
    private static GameObject UIButton => instance.uiButton;
    private static Transform GridParent => instance.gridParent;
    private static Text PlayerText => instance.playerText;

    private static Dictionary<int, UICellButton> uiButtonsDict = new Dictionary<int, UICellButton>();

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        SpawnUICellButtons();
    }

    public static void Show()
    {
        Canvas.enabled = true;
    }

    public static void Hide()
    {
        Canvas.enabled = false;
    }

    private static void SpawnUICellButtons()
    {
        for (int i = 0; i < GameController.CELLS_AMOUNT; i++)
        {
            UICellButton uIButton = Instantiate(UIButton.GetComponent<UICellButton>());
            uIButton.name = $"UICellButton {(i)}";
            uIButton.Init(i, GridParent);
            
            uiButtonsDict.Add(i, uIButton); // init dictionsry by index, uiCellBehaviour
        }
    }

    public static void InitUICellButtons()
    {
        for (int i = 0; i < uiButtonsDict.Count; i++)
        {
            uiButtonsDict[i].ReinitCell();
        }
    }

    public static void UpdatePlayerText(string message)
    {
        PlayerText.text = message;
    }
    
    public static Sprite GetCurrentPlayerSprite()
    {
        return GameController.IsFirstPlayerMoved() ? CrossSprite : CircleSprite;
    }

    public static void SelectedWinCells(int[] indexes)
    {
        if (indexes.Length != 3)
        {
            Debug.Log("Length should be 3");
            return;             
        }

        foreach (var key in indexes)
        {
            uiButtonsDict[key].SetImageColor(WinCellColor);
        }
    }
}
